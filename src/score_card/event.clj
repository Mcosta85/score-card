(ns score-card.event)

(def ^:private event-scores
  {"PushEvent" 5
   "PullRequestReviewCommentEvent" 4
   "WatchEvent" 3
   "CreateEvent" 2})

(defn score-events
  "Assigns a score to a sequence of events."
  [events]
  (when-not (empty? events)
    (reduce + (map #(get event-scores (:type %) 1) events))))
