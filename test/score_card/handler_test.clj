(ns score-card.handler-test
  (:require [clojure.test :refer :all]
            [score-card.event-source.protocol :refer [EventSource]]
            [score-card.handler :refer :all]))

(def ^:private events
  {:test-user [{:type "CreateEvent"}
               {:type "PushEvent"}]
   :error-user {:message "test error"
                :documentation_url "doc url"}})

(defonce ^:private event-source
  (reify EventSource
    (get-events [_ user]
      {:body ((keyword user) events)
       :status 200})))

(def ^:private sanitize-error #'score-card.handler/sanitize-error)
(def ^:private make-response #'score-card.handler/make-response)
(def ^:private calculate-score #'score-card.handler/calculate-score)
(def ^:private events-handler #'score-card.handler/events-handler)
(def ^:private score-handler #'score-card.handler/score-handler)

(deftest sanitize-error-test
  (testing "sanitize-error given a document with message"
    (let [expected {:message "test error"}
          actual (sanitize-error {:message "test error"
                                  :documentation_url "should be gone"})]
      (is (= expected actual))))
  (testing "sanitize-error given a document without message"
    (let [doc {:test "test"}
          actual (sanitize-error doc)]
      (is (= doc actual)))))

(deftest make-response-test
  (testing "make-response given a document with body and json mime"
    (let [doc {:status 200
               :body {:message "test body"}}
          expected {:status 200
                    :headers {"Content-Type" "application/json"}
                    :body {:message "test body"}}
          actual (make-response doc "application/json")]
      (is (= expected actual))))

  (testing "make-response given a string and text mime"
    (let [expected {:status 200
                    :headers {"Content-Type" "text/plain"}
                    :body "test string"}
          actual (make-response {:status 200
                                 :body "test string"}
                                "text/plain")]
      (is (= expected actual)))))

(deftest calculate-score-test
  (testing "calculate-score given 3 create events"
    (let [expected 6
          actual (calculate-score {:body [{:type "CreateEvent"}
                                          {:type "CreateEvent"}
                                          {:type "CreateEvent"}]})]
      (is (= expected actual))))

  (testing "calculate-score given an event of each type"
    (let [expected 14
          actual (calculate-score {:body [{:type "PushEvent"}
                                          {:type "PullRequestReviewCommentEvent"}
                                          {:type "WatchEvent"}
                                          {:type "CreateEvent"}]})]
      (is (= expected actual)))))

(deftest events-handler-test
  (testing "events-handler given mock source and test user."
    (let [result (events-handler event-source "test-user")]
      (is (= 200 (:status result)))
      (is (= "application/json" (get-in result [:headers "Content-Type"])))
      (is (= {:events [{:type "CreateEvent"}
                       {:type "PushEvent"}]}
             (:body result)))))

  (testing "events-handler given mock source and error user"
    (let [result (events-handler event-source "error-user")]
      (is (= {:message "test error"} (:body result))))))

(deftest score-handler-test
  (testing "score-handler given mock source and test user"
    (let [{:keys [body headers] :as d}
          (score-handler event-source "test-user")]
      (is (= "test-user's score is 7" body))
      (is (= "text/plain" (get headers "Content-Type")))))

  (testing "score-handler given mock source and error user"
    (let [result (score-handler event-source "error-user")]
      (is (= {:message "test error"} (:body result))))))
