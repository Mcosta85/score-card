(ns score-card.event-source.github
  (require [score-card.event-source.protocol :refer [EventSource]]
           [org.httpkit.client :as http]
           [clojure.data.json :as json]
           [clojure.tools.logging :as log]))

(def ^:private event-template
  "https://api.github.com/users/%s/events")

(defrecord GithubEventSource
  []

  EventSource
    
  (get-events [_ user]
    (log/infof "getting %s's events" user)
    (let [url (format event-template user)
          evs (http/get url)]
      (select-keys (update @evs :body #(json/read-str % :key-fn keyword))
                   [:body :status]))))
