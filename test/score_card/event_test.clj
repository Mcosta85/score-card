(ns score-card.event-test
  (:require [clojure.test :refer :all]
            [score-card.event :as event]))

(deftest score-events-test
  (testing "score-events given a seq of events"
    (let [expected 2
          actual (event/score-events [{:type "CreateEvent"}])]
      (is (= expected actual))))

  (testing "score-events given empty seq"
    (is (empty? (event/score-events [])))))
