(ns score-card.event-source.protocol)

(defprotocol EventSource
  "Defines an interface for any providers of event details."

  (get-events [this user] "Return a seq of event data."))
