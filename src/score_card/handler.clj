(ns score-card.handler
  (:require [clojure.tools.logging :as log]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [environ.core :as environ]
            [org.httpkit.server :as http]
            [ring.middleware.json :refer [wrap-json-response]]
            [ring.util.response :as ring]
            [score-card.event :as event]
            [score-card.event-source.protocol :as evproto]
            [score-card.event-source.github])
  (:import [score_card.event_source.github GithubEventSource]))

(defonce server (atom nil))

;; Instantiates an instance of the Github Evenet Source provider.
(defonce ^:private event-source (GithubEventSource.))
                                 
(def ^:private blacklisted-fields
  [:documentation_url])

(defn- sanitize-error
  "Removes extra fields from error response. These field lead to details of the system leaking to the user."
  [doc]
  (if (:message doc)
    (apply dissoc doc blacklisted-fields)
    doc))

(defn- make-response
  "Wraps the data in a ring reponse with approriate headers."
  [{:keys [body status] :as doc} mime]
  (-> (or
       body doc)
      ring/response
      (ring/content-type mime)
      (ring/status status)))

(defn- calculate-score
  "Calculates the score for the given users events stream."
  [events]
  (if-not (get-in events [:body :message])
    (event/score-events (:body events))
    events))

(defn- events-handler
  "Formats the users events for display."
  [event-source user]
  (let [events (evproto/get-events event-source user)]
    (if-not (get-in events [:body :message])
      (make-response (merge events
                            {:body {:events (:body events)}})
                     
                     "application/json")
      (update events :body sanitize-error))))

(defn- score-handler
  "Formats the output of calculating the event score."
  [event-source user]
  (let [events (evproto/get-events event-source user)
        score (calculate-score events)]

    (if (:body score)
      (update score :body sanitize-error)
      (make-response (format "%s's score is %s" user score)
                     "text/plain"))))

(defroutes app-routes
  "Returns all events for the user."
  (GET "/users/:user/events" [user] (events-handler event-source user))
                                             
  (GET "/users/:user/score" [user] (score-handler event-source user))

  (route/not-found "Not Found"))

(def app
  (-> app-routes
      wrap-json-response))

(defn stop-server!
  []
  (when-not (nil? @server)
    (reset! server nil)))

(defn -main
  []
  (log/infof "Starting server on port %s" 3000)
  (reset! server (http/run-server #'app {:port 3000})))
