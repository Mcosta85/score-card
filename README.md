# Score Card #

This repository contains code for RentPath's score card assignment

### How do I get set up? ###

* Clone the repo
* Run 'lein run' or launch a repl and execute (-main) in the score-card.handler ns
* Send a GET request to localhost:3000/users/<github-name>/events
* Send a GET request to localhost:3000/users/<github-name>/score
* Run 'lein test' to execute the unit tests